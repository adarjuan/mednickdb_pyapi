import requests
import json
import datetime
import re
import time
import dateutil.parser
import sys
import pandas as pd
from collections import OrderedDict
from operator import itemgetter
import numpy as np
from typing import List, Union, Tuple, Dict


class ServerError(Exception):
    """Custom error when server throws something nasty"""
    pass


class ResponseError(Exception):
    """Custom error when server doesnt return what we think it should"""
    pass


param_map = {
    'fid':'id',
}



# Dict to help convert human readable queries into mongo-esqe queries handeled by backend

query_kwmap = OrderedDict({
    ' and ': '&',
    ' or ': '*OR*',
    ' >= ': '=*GTE*',
    ' > ': '=*GT*',
    ' <= ': '=*LTE*',
    ' < ': '=*LT*',
    ' not in ': '=*NIN*',
    ' in ': '=*IN*',
    ' not ': '=*NE*',
    ' != ': '=*NE*',
    ' = ': '=',
    '==': '=',
    '>=': '=*GTE*',
    '>': '=*GT*',
    '<=': '=*LTE*',
    '<': '=*LT*',
    '!=': '=*NE*',
    ' & ': '&',
    ' | ': '*OR*',
    '|': '*OR*',
    '*=*':'**', #remove the extra = added after or
})


class MyEncoder(json.JSONEncoder):
    """Custom JSON encoding for converting datetimes to time since epoch, and np types to simple python types"""
    def default(self, obj):
        """
        :param obj: Object which should be converted
        :return: Converted object
        """
        if isinstance(obj, (datetime.datetime, datetime.date)):
            return time.mktime(obj.timetuple())*1000  # Convert to ms since epoch
        elif isinstance(obj, np.integer):  # TODO test
            return int(obj)
        elif isinstance(obj, np.floating):
            return float(obj)
        elif isinstance(obj, np.ndarray):
            return obj.tolist()
        else:
            return json.JSONEncoder.default(self, obj)


class MyDecoder(json.JSONDecoder):
    def __init__(self, *args, **kargs):
        """
        Creates a decoder object to add custom decoding on JSON encoded strings
        :param args: see parent object
        :param kargs: as above
        """
        json.JSONDecoder.__init__(self, object_hook=self.parser,
                                  *args, **kargs)

    def parser(self, dct):
        """
        Custom JSON decoder. Parses known date fields and strings that look like datetimes to python datetime.

        :param dct: Object to parse
        :return: parsed dict object
        """
        for k, v in dct.items():
            if isinstance(v, str) and v == '':
                dct[k] = None
            if k in ['datemodified','dateexpired']: #TODO other dates? anything with the str "date" in it?
                dct[k] = datetime.datetime.fromtimestamp(v/1000)
            # Parse datestrings back to python datetimes
            if isinstance(v, str) and re.search('[0-9]*-[0-9]*-[0-9]*T[0-9]*:[0-9]*:', v):
                try:
                    dct[k] = dateutil.parser.parse(v)
                except:
                    pass
        return dct


def _json_dumps(obj):
    def _convert_np_objects(dict_to_convert):
        new = {}
        for k, v in dict_to_convert.items():
            if isinstance(v, dict):
                new[k] = _convert_np_objects(v)
            else:
                if isinstance(v, float):
                    if np.isnan(v):
                        new[k] = None
                    elif np.isneginf(v):
                        new[k] = -1e256
                    elif np.isposinf(v):
                        new[k] = 1e256
                    else:
                        new[k] = v
                else:
                    new[k] = v
        return new

    return json.dumps(_convert_np_objects(obj), cls=MyEncoder)


def _json_loads(ret, file=False):
    """
    Helper function to load JSON return object from the requests lib into python objects.
    Will log exceptions and print extra information when server side exceptions are returned.
    :param ret: Returned object from requests lib (from get or post)
    :param file: If a file is returned, set as true. Will return file, and not try to decode from JSON.
    :return: File is file is true, decoded json if no file
    :except: Raises error in python if server error detected.
    """
    try:
        ret.raise_for_status()
    except requests.exceptions.HTTPError as e:
        raise ServerError('Server Replied "' + ret.content.decode("utf-8") + '"') from e
    if ret.status_code not in [200, 201]:
        raise ResponseError('Server responded with failure code:', ret.reponse.status_code)
    if file:
        return ret.content
    else:
        return json.loads(ret.content, cls=MyDecoder)


def _parse_locals_to_data_packet(locals_dict):
    """
    Takes the locals object (i.e. function inputs as a dict), maps keys from.
    TODO retire this function, its pretty hacky
    :param locals_dict:
    :return: parsed locals object
    """
    if 'self' in locals_dict:
        locals_dict.pop('self')
    if 'kwargs' in locals_dict:
        kwargs = locals_dict.pop('kwargs')
        locals_dict.update(kwargs)
    return {(param_map[k] if k in param_map else k): v for k, v in locals_dict.items() if v is not None}


class MednickAPI:

    def __init__(self, username, password, server_address='http://saclab.ss.uci.edu:8000', debug=False):

        """
        server_address address constructor,
        debug option will print detailed request and response
        information for most methods
        """
        self.server_address = server_address
        self.s = requests.session()
        self.debug = debug
        self.login(username, password)
        print('Successfully connected', username, 'to server at', self.server_address, 'with', self.usertype, 'privileges')

    @staticmethod
    def format_as(ret_data, format='dataframe_single_index'):
        """
        Format a return data from database into useful formats
        :param ret_data: Data to format, as a list of dicts
        :param format: Format to return, one of:
         - nested_dict: standard nested object, i.e. do not apply formatting
         - flat_dict: remove keys in "data", and flatten so each var nested in each key of data is key.var
            example:
                [{'data':{'demographics':{'age':22, 'sex':'M'}}}] --> [{'demographics.age':22, 'demographics.sex':'M'}]
         - dataframe_single_index: If a list with single dict supplied, format as flat_dict,
                                    but convert to pd.Dataframe with keys as indexs.
                                   If a list with multiple dict supplied, format as flat_dict,
                                    but convert to pd.Dataframe with keys as indexs, and stack each dict in list.
         - dataframe_multi_index: TODO
        :return Data formatted as specified
        """
        if format == 'nested_dict':
            return ret_data

        assert isinstance(ret_data, list), """Input of format_as must be list, wrap single objects like [object]"""

        row_cont = []
        row_cont_dict = []
        for row in ret_data:
            if 'data' in row:
                for datatype, datadict in row.pop('data').items():
                    row.update({datatype+'.'+k: v for k, v in datadict.items()})
            row_cont_dict.append(row)
            row_cont.append(pd.Series(row))

        if format == 'flat_dict':
            return row_cont_dict

        df = pd.concat(row_cont, axis=1).T
        if format == 'dataframe_single_index':
            return df
        elif format == 'dataframe_multi_index':
            raise NotImplementedError('TODO')
        else:
            ValueError('Unknown format requested, can be single_index or multi_index')


    @staticmethod
    def extract_var(list_of_dicts, var, raise_on_missing=True):
        """
        Helper function to extract a list of values from a list of dicts
        example:
            extract_var([{'a':1, 'b':11}, {'a':2, 'b':22}]) == [1,2]

        :param list_of_dicts: A list of dictionaries, such as file_info objects
        :param var: The key of the variable to extract
        :param raise_on_missing: If true, raise an error when the var is missing from any of the dicts in the list
        :return: Returns a list of the values of the var for each dict
        """
        if raise_on_missing:
            return [d[var] for d in list_of_dicts]
        else:
            return [d[var] for d in list_of_dicts if var in d]

    @staticmethod
    def sortby(sort_x, by_key, reverse=True):
        """
        Sorts a list of dictionaries (e.g. file_info objects) by a specific key
        :param sort_x: list of dicts/objects to sort
        :param by_key: key to sort by
        :param reverse: if sorting should be reversed
        :return: sorted list of dicts
        """
        return sorted(sort_x, key=itemgetter(by_key), reverse=reverse)

    # @staticmethod // TODO remove function
    # def discard_subsets(object_list):
    #     """
    #     From a list of objects, remove the objects that are complete subsets of other objects.
    #     This does not check data, just ['studyid', 'versionid', 'subjectid','visitid','sessionid'] keys.
    #     For example, {'studyid':'TEST', 'versionid':1} is a subset of {'studyid':'TEST', 'versionid':1, 'subjectid':1}
    #     and therefore would be removed from the list
    #     :param object_list: The list of objects to remove subsets from.
    #     :return: the object_list with subsets removed
    #     """
    #     hierarchical_specifiers = ['studyid', 'versionid', 'subjectid','visitid','sessionid']
    #     for subset_idx in range(len(object_list) - 1, -1, -1): # iterate backwards so we can drop items but dont bugger the indexes
    #         candidate_subset = object_list[subset_idx]
    #         for superset_idx in range(len(object_list) - 1, -1, -1):
    #             candidate_superset = object_list[superset_idx]
    #             if subset_idx == superset_idx: # compare int faster than compare dict
    #                 continue
    #             if all((k not in candidate_subset) or (candidate_subset[k] is None or candidate_subset[k] == candidate_superset[k])
    #                    for k in hierarchical_specifiers):
    #                 del object_list[subset_idx]
    #                 break
    #     return object_list

    def login(self, username, password):
        """
        Login to the server. Saves the login token.
        :param username: username to login with (generally an email)
        :param password: password
        :raises ServerError when server throws error, ResponseError when server returns something other than 200
        """
        self.username = username
        base_str = self.server_address + '/auth/login'
        data = {'email':username, 'password':password}
        ret = _json_loads(self.s.post(base_str, data=data))
        if not ret['authenticated']:
            raise PermissionError("Login failed, unrecognized username, password combination")
        self.usertype = ret['permission']

    # File related functions
    def upload_file(self, fileobject, fileformat, filetype, fileversion=None, studyid=None, versionid=None, subjectid=None,
                    visitid=None, sessionid=None):
        """
        Upload a file data to the filestore in the specified location.
        If this is a brand new file, then add, if it exists, then overwrite (i.e. set previous version as inactive).
        Returns file info object
        :param fileobject: The file object, i.e. the return from open('filename.csv','r+')
        :param fileformat: Format of the file, this dictates how the file will be parsed by microservices (pyparse).
            Known parse-able fileformats are currently (others will be ignored by parsing microservices):
            - "sleep_scoring" - sleep scoring files. Currently supports edf, mat (hume), xml (NSRR), and various tabular types
            - "tabular" - any tabular-like data, with column headers and cols for specific subjectid, visitid, etc
            - "eeg" - edf's or other eeg like files. Basically anything the python package MNE can open
            TODO:
            - "actigraphy"
            - "sleep_diaries"
        :param filetype: The "datatype" contained in the file, e.g. "demographics" for demographics related file, etc.
            Can be anything, but preferred filetypes are:
             - "sleep_eeg" for all edf, eeg, timeseries containing sleep eeg
             - "sleep_scoring" for all sleep scoring files (vrmk, mat, csv)
             - "demographics" for all demographics information (age, sex, etc)
             - "counterbalance" for a counterbalance assigning subjects/visits/sessions to conditions
             - "sleep_diary" for all sleep diary information
             - "sleep_features" for any files with spindle, REM, SO events
             - "sleep_stats" for all traditional sleep stats (minutes in REM, latency, etc)
             - Task Names ("WPA", etc)

        :param fileversion: Upload with a specific fileversion. This is usualy managed by the backend. Does this even work? FIXME
        :param studyid: specifies location in database to upload to
        :param versionid: specifies location in database to upload to
        :param subjectid: specifies location in database to upload to
        :param visitid: specifies location in database to upload to
        :param sessionid: specifies location in database to upload to
        :return: The file_info of the uploaded object
        :raises ServerError when server throws error, ResponseError when server returns something other than 200
        """
        data_packet = _parse_locals_to_data_packet(locals())

        fileobject = data_packet.pop('fileobject')
        files = {'fileobject': fileobject}

        filename = re.sub('(.*\/)', '', fileobject.name)
        data_packet['filename'] = filename

        if not self.debug:
            ret = self.s.post(url=self.server_address + '/files/upload', data={'data':_json_dumps(data_packet)}, files=files)
            #ret = self.s.post(url=self.server_address, data={'data':json.dumps(data_packet, cls=MyEncoder)}, files=files)

        else:
            #req = requests.Request('POST', self.server_address+'/files/upload', data=data_packet, files=files)

            req = requests.Request('POST', self.server_address+'/files/upload', data={'data':_json_dumps(data_packet)}, files=files)



            #req = requests.Request('POST', self.server_address ,data=data_packet, files=files)
            prep = self.s.prepare_request(req)
            print('request details:')
            print('\n'.join("%s: %s" % item for item in vars(req).items()))
            print("prep details:")
            print(prep.method,prep.headers,prep.body, sep="\n")
            #pprint(', '.join("%s: %s" % item for item in vars(prep).items()))
            ret = self.s.send(prep)
            print("upload status:", ret.status_code)

        return _json_loads(ret)['ops'][0] #ops are where the file_info for the uploaded files are stored

    def update_file_info(self, fid, **kwargs):

        """
        Change the location of a file on the datastore and update its info.
        :param fid: fid of file to update
        :param kwargs: a list of keys and values to update with,
            e.g. update_file_info(fid, studyid='TEST') or update_file_info(fid, {'studyid':'TEST'})
        :return: Updated file info.
        :raises ServerError when server throws error, ResponseError when server returns something other than 200
        """

        raise NotImplementedError("not implemented")
        data_packet = _parse_locals_to_data_packet(locals())
        if not self.debug:
            ret = self.s.put(url=self.server_address + '/files/update', data=data_packet)
        else:
            req = requests.Request('PUT', self.server_address+'/files/update', data=data_packet)

            #req = requests.Request('POST', self.server_address ,data=data_packet, files=files)
            prep = self.s.prepare_request(req)
            print('request details:')
            print('\n'.join("%s: %s" % item for item in vars(req).items()))
            print("prep details:")
            print(prep.method, prep.headers, prep.body, sep='\n')
            #print(', '.join("%s: %s" % item for item in vars(prep).items()))
            ret = self.s.send(prep)
        return _json_loads(ret) #TODO should return file info


    def update_parsed_status(self, fid: str, status: bool=True):
        """
        Change the parsed status of a file. Status is True when parsed or False otherwise
        The data part of the request is nested, same with upload_file
        :param fid: the fid of the file to change
        :param status: The status (True | false) to change to  FIXME as of 1.2.2 this function does not take status, and can only go from false->true
        :return: None
        :raises ServerError when server throws error, ResponseError when server returns something other than 200
        """

        if not self.debug:
            ret = self.s.put(url=self.server_address + '/files/updateParsedStatus', data={'data':_json_dumps({'id':fid, 'status':status})})
        else:
            req = requests.Request('PUT', self.server_address + '/files/updateParsedStatus',
                                   data={'data':_json_dumps({'id':fid, 'status':status})})

            # req = requests.Request('POST', self.server_address ,data=data_packet, files=files)
            prep = self.s.prepare_request(req)
            print('request details:')
            print('\n'.join("%s: %s" % item for item in vars(req).items()))
            print("prep details:")
            print(prep.method, prep.headers, prep.body, sep='\n')
            # print(', '.join("%s: %s" % item for item in vars(prep).items()))
            ret = self.s.send(prep)
            # JH upload DEBUG
        print("upload status:", ret.status_code)
        return _json_loads(ret)

    def delete_file(self, fid: str, delete_all_versions: bool=False,
                    reactivate_previous: bool=False,
                    remove_associated_data: bool=False):
        """
        Delete a file from the filestore.

        :param fid: the fid of the file to delete
        :param delete_all_versions: If true, delete all version of this file
        :param reactivate_previous: If true, set any old versions as the active version, and trigger a reparse of these files so there data is added to the datastore
        :param remove_associated_data: If true, purge datastore of all data associated with this file
        :return None
        :raises ServerError when server throws error, ResponseError when server returns something other than 200
        """
        locals_vars = locals().copy()
        name_map = {
            'reactivate_previous': 'previous',
            'delete_all_versions': 'all',
            'remove_associated_data': 'data',
            'fid': 'id'
        }
        locals_vars.pop('self')
        data = {name_map[k]: v for k, v in locals_vars.items()}
        _json_loads(self.s.post(self.server_address + '/files/expire', data=data)) #check for error


    def get_files(self, query: str=None, previous_versions: bool=False, format: str='nested_dict', **kwargs):
        """
        Retrieves a list of file info from files in the file store that match the above specifiers.
           When querying, any keys in the file profile may be included, and only matching files for all will be returned.
           Return file info's are sorted by datemodified
        :param query: str version of a query, supports the following operators, see unittest for more examples

            ' and '  - and together operations ('subjectid==1 and studyid==TEST'), i.e. both operands must eval to true
            ' or '  - or together operations ('subjectid==1 or studyid==TEST'), i.e. one operands must eval to true
            ' >= '  - greater than equal to, e.g. 'subjectid >= 1'
            ' > '  - greater than, e.g. 'subjectid > 1'
            ' <= '  - less than equal to, e.g. 'subjectid <= 20'
            ' < ' - less than, e.g. 'subjectid < 20'
            ' not in ' - key not in list, e.g. 'subjectid not in [20,21,22]'
            ' in ' - key in list, e.g. 'subjectid in [20,21,22]'
            ' not ' - key not equal to, e.g. 'subjectid not 20'
            ' != ' same as not
            ' = ' key is value, e.g. subjectid == 20
            ' == ' same as above
            ' & ' and together operations ('subjectid==1 & studyid==TEST') -> ('subjectid==1 and studyid==TEST')
            ' | ' or together operations ('subjectid==1 | studyid==TEST') -> ('subjectid==1 or studyid==TEST')

            All operands can be with or without a single whitespace either side

        :param previous_versions: Whether to return previous, non-active versions of the file also
        :param format: Format to return as, see format_as for possibilities
        :param kwargs: alternative way to query params, e.g. get_files(studyid='TEST') or get_files(kwargs={'studyid':'TEST'})
        :return: a list/dataframe of file_info objects that match
        :raises ServerError when server throws error, ResponseError when server returns something other than 200
        """
        if query:
            for k, v in query_kwmap.items():
                query = query.replace(k, v)
            if previous_versions:
                ret = _json_loads(self.s.get(self.server_address + '/files?'+query, params={'versions': '1'}))
            else:
                ret = _json_loads(self.s.get(self.server_address + '/files?'+query))
        else:
            params = _parse_locals_to_data_packet(kwargs)
            if previous_versions:
                params.update({'versions': '1'})
            ret = _json_loads(self.s.get(self.server_address + '/files', params=params))

        ret = self.sortby(ret, 'datemodified')

        if len(ret) > 0:
            ret = self.format_as(ret, format)

        return ret

    def get_file_by_fid(self, fid: str):
        """
        Get the file_info associated with a file id (i.e. the data associated with this id in the filestore)
        :param fid: the fid to get for
        :return: file_info associated with fid
        :raises ServerError when server throws error, ResponseError when server returns something other than 200
        """
        data_packet = _parse_locals_to_data_packet(locals())
        return _json_loads(self.s.get(url=self.server_address + '/files/info', params={'id': fid}))

    def download_file(self, fid: str):
        """
        Downloads the binary data for that fid, can be saved to disk as ```open("filename.txt", "wb").write(download_file(fid))```
        :param fid: the fid of the file to download
        :return: binary data of file
        :raises ServerError when server throws error, ResponseError when server returns something other than 200
        """
        if not self.debug:
            resp = self.s.get(url=self.server_address + '/files/download', params={'id': fid})
        else:
            req = requests.Request('GET', self.server_address + '/files/download', params={'id': fid})

            prep = self.s.prepare_request(req)
            print('request details:')
            print('\n'.join("%s: %s" % item for item in vars(req).items()))
            print("prep details:")
            print(prep.method, prep.headers, prep.body, sep='\n')
            resp = self.s.send(prep)
            print('raw content')
            print(resp.status_code,'\n',resp.text)
        return _json_loads(resp, file=True)

    def download_files(self, fids):
        """
        Downloads a number of files from a list of file id's
        :param fids: list of fids to download for
        :return: a zipped list of file binaries
        :raises ServerError when server throws error, ResponseError when server returns something other than 200
        """
        fids_param = '*AND*'.join(fids)
        if not self.debug:
            ret = self.s.get(url=self.server_address + '/files/downloadmultiple', params={'id': fids_param})
        else:
            req = requests.Request('GET', self.server_address + '/files/downloadmultiple',
                                   params={'id': fids_param})

            # req = requests.Request('POST', self.server_address ,data=data_packet, files=files)
            prep = self.s.prepare_request(req)
            print('request details:')
            print('\n'.join("%s: %s" % item for item in vars(req).items()))
            print("prep details:")
            print(prep.method, prep.headers, prep.body, sep='\n')
            # print(', '.join("%s: %s" % item for item in vars(prep).items()))
            ret = self.s.send(prep)
            # JH upload DEBUG
        print("upload status:", ret.status_code)
        return _json_loads(ret)

    def delete_multiple(self, fids):

        """
        Deletes a list of files corresponding to the given fids.
        :param fids: list of fids to delete
        :return: None
        :raises ServerError when server throws error, ResponseError when server returns something other than 200
        """


        for fid in fids:
            self.delete_file(fid=fid)

    def get_deleted_files(self):
        """
        Retrieves a list of fids for deleted files from the file store, no querying to file these files is done (TODO)
        :return: A huge list of all the file_infos of the files that have been deleted
        :raises ServerError when server throws error, ResponseError when server returns something other than 200
        """
        return _json_loads(self.s.get(url=self.server_address + '/files/expired'))

    def get_unparsed_files(self, previous_versions=False):
        """
        Return a list of fid's for unparsed files
        :param previous_versions: if true include previous versions. FIXME this would be better as an actual backend option
        :return: file_infos of unparsed files
        :raises ServerError when server throws error, ResponseError when server returns something other than 200
        """
        files = _json_loads(self.s.get(self.server_address + '/files/unparsed'))
        if not previous_versions:
            files = [file for file in files if file['active']]
        return files


    def get_parsed_files(self, previous_versions=False):
        """
        Return a list of fid's for parsed files
        :param previous_versions: if true include previous versions. FIXME this would be better as an actual backend option
        :return: file_infos of parsed files
        :raises ServerError when server throws error, ResponseError when server returns something other than 200
        """
        files = _json_loads(self.s.get(self.server_address + '/files/parsed'))
        if not previous_versions:
            files = [file for file in files if file['active']]
        return files


    def get_unique_var_values(self, var, store, **kwargs):
        """
        Get possible values of a hierarchical specifier variable from either data or files store.
        For example, get all filetypes for studyid=TEST from file store:
            get_unique_var_values('filetype', store='files', studyid='TEST') = [demographics, sleep_scoring, memtesta]
        :param var: variable to get unique values for, e.g.
        :param store: store to get data from (data or files)
        :param kwargs: specific place to search at, i.e. subjectid=1, studyid='TEST'
        :return: unique values of that variable, or empty if that variable does not exist
        :raises ServerError when server throws error, ResponseError when server returns something other than 200
        """
        if store == 'data':
            ret = self.get_data(format='nested_dict', **kwargs)
        elif store == 'files':
            ret = self.get_files(format='nested_dict', **kwargs)
        else:
            raise ValueError('Store Unknown')

        if store == 'data' and var == 'filetype':
            return self.get_data_filetypes(**kwargs)
        else:
            values = []
            for row in ret:
                try:
                    values.append(row[var])
                except KeyError:
                    values.append(None)
        return list(np.unique(values))


    def get_data_filetypes(self, include_expired: bool=False, query=None, **kwargs) -> List[str]:
        """
        Get the possible filetypes associated with a level of the hierarchy from the data store
        :param include_expired: whether or not to include expired files
        :param kwargs: hierarchical specifiers as key:value pairs
        :return: Filetypes at that level of the hierarchy
        """
        if query:
            query = '?' + self._parse_query(query)
        else:
            query = ''

        if include_expired:
            # raise NotImplementedError('include expired not implemented')
            kwargs['include_expired'] = include_expired

        return _json_loads(self.s.get(self.server_address + '/data/filetypes'+query, params=kwargs))


    # Data Functions
    def upload_data(self, data: dict, studyid, versionid, filetype, fid, subjectid, visitid=None, sessionid=None):
        """
        Upload a data to the datastore in the specified location.
            Specifiers like studyid etc contained in the data object will be extracted and used before any in the function arguments.
            If this is a new location (no data exists), then add, if it exists, merge or overwrite.

        :param data: Single level object of key:values and convertable to json.
        :param studyid: where to put on the data store
        :param versionid: where to put on the data store
        :param filetype: The type of data, see upload_file for standard values
        :param fid: If this data came from a particular file in the server, then add a file id to link back to that file
        :param subjectid: where to put on the data store
        :param visitid: where to put on the data store
        :param sessionid: where to put on the data store
        :return: file_info of uploaded data? TODO change to return the whole data object
        :raises ServerError when server throws error, ResponseError when server returns something other than 200
        """
        data_packet = _parse_locals_to_data_packet(locals())
        data_packet['sourceid'] = data_packet.pop('id')
        if not self.debug:
            response = _json_loads(self.s.post(self.server_address + '/data/upload', data={'data': _json_dumps(data_packet)}))
        else:
            resp = self.s.post(self.server_address + '/data/upload', data={'data': _json_dumps(data_packet)})
            print('raw content')
            print(resp.text)
            response = _json_loads(resp)
        #return response[1]['ops'][0]
        return response


    def _parse_query(self, query):
        query = query.replace(' = ', '=')
        start_filetype_pos = query.find('filetype')
        end_filetype_pos = start_filetype_pos + len('filetype')
        if start_filetype_pos != -1:
            start_filetype_value_pos = end_filetype_pos + query[end_filetype_pos:].find('=') + 1
            end_of_value_offset = query[start_filetype_value_pos:].find(' ')
            if end_of_value_offset == -1:
                end_filetype_value_pos = len(query)
            else:
                end_filetype_value_pos = start_filetype_value_pos + end_of_value_offset
            filetype_value = query[start_filetype_value_pos:end_filetype_value_pos].lstrip(' ').rstrip(' ')
            query = query.replace(filetype_value, '').replace('filetype=', 'data.' + filetype_value + '=*exists*')
        for k, v in query_kwmap.items():
            query = query.replace(k, v)
        return query


    def get_data(self, query=None, discard_subsets=True, format='dataframe_single_index', **kwargs):
        """
        Get all data profiles in the data store at the specified location.
        :param query: Query to filter data. See get_files for usecases.
        :param discard_subsets: Whether to remove profiles that are nested subsets of others (i.e. returned row is unique)
        :param format: how to format the data, see format_as for use.
        :param kwargs: where to search data at, same as query, but in dict form. See get_files for usecases.
        :return: data profiles that match in format as specified by format_as
        :raises ServerError when server throws error, ResponseError when server returns something other than 200
        """
        if query:
            query = self._parse_query(query)
            if self.debug:
                print("debug req info\n","get /data?"+query)
            if not discard_subsets:
                query += '&discard_subsets=false'

            rows = _json_loads(self.s.get(self.server_address + '/data?'+query))
        else:
            if 'filetype' in kwargs:
                kwargs['data.'+kwargs.pop('filetype')] = '*exists*'
            if not discard_subsets:
                kwargs['discard_subsets'] = 'false'

            params = _parse_locals_to_data_packet(kwargs)

            rows = _json_loads(self.s.get(self.server_address + '/data', params=params))
        if self.debug:

            print("debug res info:\n",rows)

        if len(rows) > 0:
            rows = self.format_as(rows, format=format)

        return rows

    def delete_data(self, **kwargs):
        """
        Delete all data at a particular level of the hierarchy or using a specific dataid given
        the data id of the data object (returned from get_data as "_id")

        :param kwargs: Where to delete data, e.g. delete_data(studyid=TEST), delete all data with SubjectID=TEST,
            if id in kwargs, then delete that specific profile with mongo id==id
        :return None
        :raises ServerError when server throws error, ResponseError when server returns something other than 200
        """
        delete_param_name = 'id'
        if delete_param_name in kwargs:
            _json_loads(self.s.delete(self.server_address + '/data/expire', data={delete_param_name: kwargs[delete_param_name]}))
        else:
            rows = self.get_data(format='nested_dict', discard_subsets=False, **kwargs)
            for row in rows:
                self.delete_data(id=row['_id'])

    def get_data_from_single_file(self, filetype, fid, format='dataframe_single_index'):
        """
        Get the data in the datastore associated with a file (i.e. get the data that was extracted from that file on upload)

        :param filetype: The filetype of the data to get #FIXME this could be pulled from the file itself after a query to the filestore?
        :param fid: The file which generated the data you want to get back
        :param format: Return format. See format_as
        :return: the data profiles where the parsing of that file added data
        """
        # TODO filter the returned object by just data that came from fid?
        return self.get_data('data.'+filetype+'.sourceid='+fid, format=format)

    def delete_data_from_single_file(self, fid):
        """
        Deletes the data in the datastore associated with a file
        (i.e. get the data that was extracted from that file on upload)
        :param fid:
        :return: None
        :raises ServerError when server throws error, ResponseError when server returns something other than 200
        """
        if not self.debug:
            ret = self.s.delete(self.server_address + '/data/expireByFile', data={'id':fid})
        else:
            req = requests.Request('DELETE', self.server_address + '/data/expireByFile',
                                   data={'id': fid})

            # req = requests.Request('POST', self.server_address ,data=data_packet, files=files)
            prep = self.s.prepare_request(req)
            print('request details:')
            print('\n'.join("%s: %s" % item for item in vars(req).items()))
            print("prep details:")
            print(prep.method, prep.headers, prep.body, sep='\n')
            # print(', '.join("%s: %s" % item for item in vars(prep).items()))
            ret = self.s.send(prep)
            # resp = self.s.delete(self.server_address + '/data/expireByFile', data={'id':fid})
            print('raw content')
            print('  ',ret.text)
            print(ret.status_code)
            # response = _json_loads(resp)
        return _json_loads(ret)


    def _trigger_reparse(self, query=None, **kwargs):
        """
        Helper function to trigger the re-parse of all files in a specific level of the hierarchy (such as for a whole study)
        Does not work b/c update_parsed_status is not fixed. #TODO https://trello.com/c/KaMewf5g
        Will support file-type matching when this is implemented #FIXME https://trello.com/c/rpQTcC9G
        Requires a unittest #TODO
        Private due to possible miss use.
        :param query: as for get_files
        :param kwargs: as for get_files
        :raises ServerError when server throws error, ResponseError when server returns something other than 200
        """
        files_to_reparse = self.get_files(query=query, **kwargs)
        for file_info in files_to_reparse:
            self.update_parsed_status(file_info['_id'], status=False)


    def _delete_all_files(self, password):
        """
        Delete all files on the DB, use with extreme caution. Do you really need to use this?
        Private due to possible miss use.
        :param password: the password to use this program
        :return: None
        :raises ServerError when server throws error, ResponseError when server returns something other than 200
        """
        if password == 'is_this_really_the_best_option?':
            files = self.get_files()
            print(len(files), 'found, beginning delete of ALL FILES on the server...')
            for file in files:
                print(self.delete_file(file['_id']))
        else:
            print('Cannot delete all files on the server without correct password!')

    def __del__(self):
        """
        Triggers logout. Do we need this? FIXME NotImplemented
        :return:
        """
        # TODO, this should trigger logout? or Token deletion?
        pass


if __name__ == '__main__':
    pass
