import sys
import os
import os.path
my_path = os.path.dirname(os.path.abspath(__file__))
sys.path.append(my_path+'/../')
from mednickdb_pyapi.mednickdb_pyapi import MednickAPI
import pytest
import time
import pandas as pd
import pprint
pp = pprint.PrettyPrinter(indent=4)

server_address = 'http://saclab.ss.uci.edu:8000'

file_update_time = 2
data_update_time = 10

user = 'mednickdb.testing@gmail.com' #TODO pull from env when we have real auth
password = os.environ['MEDNICKDB_DEFAULT_PW']

data_upload_working = False

@pytest.fixture(scope="module")
def mednickAPI_setup():
    return MednickAPI(user, password)


def dict_issubset(superset, subset, show_diffs=False):
    if show_diffs:
        return [item for item in subset.items() if item not in superset.items()]
    return all(item in superset.items() for item in subset.items())


def pytest_namespace():
    return {'usecase_1_filedata': None}


def test_clear_test_study():
    """
    Clear all data and files with the studyid of "TEST". This esentually refreshes the database for new testing.
    """
    med_api = mednickAPI_setup()
    fids = med_api.extract_var(med_api.get_files(studyid='TEST'), '_id')
    if fids:
        for fid in fids:
            med_api.delete_file(fid, delete_all_versions=True)
            med_api.delete_data_from_single_file(fid)
        fids2 = med_api.extract_var(med_api.get_files(studyid='TEST'),'_id')
        assert fid not in fids2
        assert (fids2 == [])
        deleted_fids = med_api.extract_var(med_api.get_deleted_files(),'_id')
        assert all([dfid in deleted_fids for dfid in fids])
    med_api.delete_data(studyid='TEST')
    assert len(med_api.get_data(studyid='TEST', format='nested_dict')) == 0 #TODO after clearing up sourceid bug


@pytest.mark.dependency(['test_clear_test_study'])
def test_usecase_1():
    """runs usecase one from the mednickdb_usecase document (fid=)"""
    #a)
    med_api = mednickAPI_setup()
    file_info_post = {
        'fileformat':'eeg',
        'studyid':'TEST',
        'versionid':1,
        'subjectid':1,
        'visitid':1,
        'sessionid':1,
        'filetype':'sleep_eeg',
    }
    file_data_real = file_info_post.copy()
    with open(os.path.join(os.path.dirname(__file__),'testfiles/sleepfile1.edf'),'rb') as sleepfile:
        file_info_returned = med_api.upload_file(fileobject=sleepfile, **file_info_post)

    with open(os.path.join(os.path.dirname(__file__),'testfiles/sleepfile1.edf'), 'rb') as sleepfile:
        downloaded_sleepfile = med_api.download_file(file_info_returned['_id'])
        assert (downloaded_sleepfile == sleepfile.read())

    # b)
    time.sleep(file_update_time)  # give db 5 seconds to update
    file_info_get = med_api.get_file_by_fid(file_info_returned['_id'])
    file_info_post.update({'filename': 'sleepfile1.edf', 'filedir': 'uploads/TEST/1/1/1/1/sleep_eeg/'})
    assert dict_issubset(file_info_get, file_info_post)

    time.sleep(data_update_time-file_update_time)  # give db 5 seconds to update
    file_datas = med_api.get_data_from_single_file(filetype='sleep_eeg', fid=file_info_returned['_id'], format='flat_dict')
    file_data_real.pop('fileformat')
    file_data_real.pop('filetype')
    file_data_real.update({'sleep_eeg.eeg_nchan': 3, 'sleep_eeg.eeg_sfreq':128, 'sleep_eeg.eeg_meas_date':1041380737000, 'sleep_eeg.eeg_ch_names': ['C3A2', 'C4A1', 'ECG']})  # add actual data in file. # TODO add all
    pytest.usecase_1_filedata = file_data_real
    pytest.usecase_1_filename_version = file_info_get['filename_version']

    assert(any([dict_issubset(file_data, file_data_real) for file_data in file_datas])), "Is pyparse running? (and working)"




@pytest.mark.dependency(['test_usecase_1'])
def test_usecase_2():
    # a)

    file_info_post = {'filetype':'demographics',
                      'fileformat':'tabular',
                      'studyid':'TEST',
                      'versionid':1}

    med_api = mednickAPI_setup()
    with open(os.path.join(os.path.dirname(__file__),'testfiles/TEST_Demographics.xlsx'), 'rb') as demofile:
        # b)
        file_info = med_api.upload_file(fileobject=demofile, **file_info_post)
        fid = file_info['_id']
        downloaded_demo = med_api.download_file(fid)
        with open(os.path.join(os.path.dirname(__file__),'testfiles/TEST_Demographics.xlsx'), 'rb') as demofile:
            assert downloaded_demo == demofile.read()

    # c)
    time.sleep(file_update_time)  # Give file db 5 seconds to update
    file_info_post.update({'filename': 'TEST_Demographics.xlsx', 'filedir': 'uploads/TEST/1/demographics/'})
    file_info_get = med_api.get_file_by_fid(fid)
    assert dict_issubset(file_info_get, file_info_post)

    # d)
    time.sleep(data_update_time-file_update_time)  # Give data db 50 seconds to update
    data_rows = med_api.get_data(studyid='TEST', versionid=1, format='flat_dict')
    correct_row1 = {'studyid': 'TEST', 'versionid': 1, 'subjectid': 1,
                    'demographics.age': 23, 'demographics.sex': 'F', 'demographics.bmi': 23}
    correct_row1.update(pytest.usecase_1_filedata)
    correct_row2 = {'studyid': 'TEST', 'versionid': 1, 'subjectid': 2,
                    'demographics.age': 19, 'demographics.sex': 'M', 'demographics.bmi': 20}
    correct_rows = [correct_row1, correct_row2]

    pytest.usecase_2_row1 = correct_row1
    pytest.usecase_2_row2 = correct_row2
    pytest.usecase_2_filename_version = file_info_get['filename_version']

    for correct_row in correct_rows:
        assert any([dict_issubset(data_row, correct_row) for data_row in data_rows]), "demographics data downloaded does not match expected"

    # e)
    data_sleep_eeg = med_api.get_data(studyid='TEST', versionid=1, filetype='sleep_eeg', format='flat_dict')[0]
    assert dict_issubset(data_sleep_eeg, pytest.usecase_1_filedata), "sleep data downloaded does not match what was uploaded in usecase 1"


@pytest.mark.dependency(['test_usecase_2'])
def test_usecase_3():

    # a)
    med_api = mednickAPI_setup()
    fid_for_manual_upload = med_api.extract_var(med_api.get_files(studyid='TEST'), '_id')[0] # get a random fid
    data_post = {'studyid': 'TEST',
                 'filetype': 'memtesta',
                 'data': {'accuracy': 0.9},
                 'versionid': 1,
                 'subjectid': 2,
                 'visitid': 1,
                 'sessionid': 1}
    log = med_api.upload_data(fid=fid_for_manual_upload, **data_post)

    # b)
    time.sleep(file_update_time)  # Give db time to update
    correct_filename_versions = [pytest.usecase_1_filename_version, pytest.usecase_2_filename_version]
    filename_versions = med_api.extract_var(med_api.get_files(studyid='TEST', versionid=1), 'filename_version')
    assert all([fid in correct_filename_versions for fid in filename_versions]), "Missing expected filename versions from two previous usecases"

    # c)
    time.sleep(data_update_time-file_update_time)  # Give db time to update
    data_rows = med_api.get_data(studyid='TEST', versionid=1, format='flat_dict')
    correct_row_2 = pytest.usecase_2_row2.copy()
    correct_row_2.update({'memtesta.accuracy': 0.9, 'visitid': 1})
    pytest.usecase_3_row2 = correct_row_2
    correct_rows = [pytest.usecase_2_row1, correct_row_2]
    for correct_row in correct_rows:
        assert any([dict_issubset(data_row, correct_row) for data_row in data_rows])




@pytest.mark.dependency(['test_usecase_3'])
def test_usecase_4():
    # a)
    med_api = mednickAPI_setup()

    # b) uploading some scorefiles
    file_info1_post = {
        'fileformat':'sleep_scoring',
        'studyid':'TEST',
        'versionid':1,
        'subjectid':2,
        'visitid':1,
        'sessionid':1,
        'filetype':'sleep_scoring'
    }
    with open(os.path.join(os.path.dirname(__file__),'testfiles/scorefile1.mat'), 'rb') as scorefile1:
        fid1 = med_api.upload_file(scorefile1,
                                   **file_info1_post)

    file_info2_post = file_info1_post.copy()
    file_info2_post.update({'visitid':2})
    with open(os.path.join(os.path.dirname(__file__),'testfiles/scorefile2.mat'), 'rb') as scorefile2:
        fid2 = med_api.upload_file(scorefile2,
                                   **file_info2_post)

    scorefile1_data = {'sleep_scoring.epochstage': ['unknown', 'unknown', 'unknown', 'wbso', 'wbso', 'wbso', 'wbso', 'wbso', 'wbso', 'wbso'],
                       'sleep_scoring.starttime': 1451635302000, 'sleep_scoring.mins_in_waso': 0, 'sleep_scoring.mins_in_stage1': 0,
                       'sleep_scoring.mins_in_stage2': 0, 'sleep_scoring.mins_in_sws': 0, 'sleep_scoring.mins_in_rem': 0,
                       'sleep_scoring.sleep_efficiency': None, 'sleep_scoring.total_sleep_time': 0}
    scorefile2_data = {'sleep_scoring.epochstage': ['wbso', 'wbso', 'stage1', 'stage1', 'stage2', 'stage2', 'sws', 'sws', 'stage2', 'stage2'],
                       'sleep_scoring.starttime': 1451635302000, 'sleep_scoring.mins_in_waso': 0, 'sleep_scoring.mins_in_stage1': 1,
                       'sleep_scoring.mins_in_stage2': 2, 'sleep_scoring.mins_in_sws': 1, 'sleep_scoring.mins_in_rem': 0,
                       'sleep_scoring.sleep_efficiency': 1.0, 'sleep_scoring.total_sleep_time': 4}

    # c)
    time.sleep(data_update_time)  # Give db 50 seconds to update
    data_rows = med_api.get_data(studyid='TEST', versionid=1, format='flat_dict')
    correct_row_1 = pytest.usecase_2_row1.copy()
    scorefile1_data.update(pytest.usecase_3_row2)
    correct_row_2 = scorefile1_data
    scorefile2_data.update(pytest.usecase_2_row2)
    correct_row_3 = scorefile2_data
    correct_rows = [correct_row_1, correct_row_2, correct_row_3]
    for correct_row in correct_rows:
        try:
            assert any([dict_issubset(data_row, correct_row) for data_row in data_rows])
        except AssertionError as e:
            print('###############Shit###############')
            raise(e)

    pytest.usecase_4_row1 = correct_row_1
    pytest.usecase_4_row2 = correct_row_2
    pytest.usecase_4_row3 = correct_row_3


@pytest.mark.dependency(['test_usecase_4'])
def test_usecase_5():
    # a)
    med_api = mednickAPI_setup()
    data_rows = med_api.get_data(query='studyid=TEST and data.memtesta.accuracy>=0.9', format='flat_dict')
    assert any([dict_issubset(data_row, pytest.usecase_3_row2) for data_row in data_rows])


def test_get_specifiers():
    med_api = mednickAPI_setup()
    sids = med_api.get_unique_var_values('studyid', store='data')
    assert 'TEST' in sids

    vids = med_api.get_unique_var_values('versionid', studyid='TEST', store='data')
    assert vids == [1]

    sids = med_api.get_unique_var_values('subjectid', studyid='TEST', store='data')
    assert sids == [1, 2]

    vids = med_api.get_unique_var_values('visitid', studyid='TEST', store='data')
    assert vids == [1, 2]

    sids = med_api.get_unique_var_values('sessionid', studyid='TEST', store='data')
    assert sids == [1]

    filetypes = med_api.get_unique_var_values('filetype', studyid='TEST', store='data')
    assert set(filetypes) == {'sleep_eeg', 'sleep_scoring', 'demographics', 'memtesta'}
