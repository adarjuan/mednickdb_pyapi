FROM python:3.6.8-slim
WORKDIR /pyapi
RUN apt-get -qq update && apt-get -qq install -y 
copy . .
RUN pip install -r requirements.txt
ARG PASSWORD
ENV MEDNICKDB_TESTING_PW $PASSWORD
ENV MEDNICKDB_TESTING_EMAIL $USERNAME
CMD ["pytest","-v","test/test_mednickdb_pyapi.py"]


